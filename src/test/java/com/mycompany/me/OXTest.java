/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.me;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class OXTest {
    
    public OXTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckVerticlePlayerOcol1Win() {
    char table[][] = {{'O', '-', '-'},
                                {'O', '-', '-'}, 
                                {'O', '-', '-'}};
    char currentPlayer = 'O';
    int  col=1;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerOcol2Win() {
    char table[][] = {{'-', 'O', '-'},
                                {'-', 'O', '-'}, 
                                {'-', 'O', '-'}};
    char currentPlayer = 'O';
    int  col=2;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerOcol3Win() {
    char table[][] = {{'-', '-', 'O'},
                                {'-', '-', 'O'}, 
                                {'-', '-', 'O'}};
    char currentPlayer = 'O';
    int  col=3;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    
    @Test
    public void testCheckHorizontalPlayerORow1Win() {
    char table[][] = {{'O', 'O', 'O'},
                                {'-', '-', '-'}, 
                                {'-', '-', '-'}};
    char currentPlayer = 'O';
    int  row=1;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckHorizontalPlayerORow2Win() {
    char table[][] = {{'-', '-', '-'},
                                {'O', 'O', 'O'}, 
                                {'-', '-', '-'}};
    char currentPlayer = 'O';
    int  row=2;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckHorizontalPlayerORow3Win() {
    char table[][] = {{'-', '-', '-'},
                                {'-', '-', '-'}, 
                                {'O', 'O', 'O'}};
    char currentPlayer = 'O';
    int  row=3;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    
    
    @Test
    public void testCheckVerticlePlayerXcol1Win() {
    char table[][] = {{'X', '-', '-'},
                                {'X', '-', '-'}, 
                                {'X', '-', '-'}};
    char currentPlayer = 'X';
    int  col=1;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerXcol2Win() {
    char table[][] = {{'-', 'X', '-'},
                                {'-', 'X', '-'}, 
                                {'-', 'X', '-'}};
    char currentPlayer = 'X';
    int  col=2;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    @Test
    public void testCheckVerticlePlayerXcol3Win() {
    char table[][] = {{'-', '-', 'X'},
                                {'-', '-', 'X'}, 
                                {'-', '-', 'X'}};
    char currentPlayer = 'X';
    int  col=3;
    assertEquals(true,OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    
    @Test
    public void testCheckHorizontalPlayerXRow1Win() {
    char table[][] = {{'X', 'X', 'X'},
                                {'-', '-', '-'}, 
                                {'-', '-', '-'}};
    char currentPlayer = 'X';
    int  row=1;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow2Win() {
    char table[][] = {{'-', '-', '-'},
                                {'X', 'X', 'X'}, 
                                {'-', '-', '-'}};
    char currentPlayer = 'X';
    int  row=2;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow3Win() {
    char table[][] = {{'-', '-', '-'},
                                {'-', '-', '-'}, 
                                {'X', 'X', 'X'}};
    char currentPlayer = 'X';
    int  row=3;
    assertEquals(true,OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    
    
    @Test
    public void testCheckPlayerOCol2Win() {
    char table[][] = {{'-', 'O', '-'},
                                {'-', 'O', '-'}, 
                                {'-', 'O', '-'}};
    char currentPlayer = 'O';
    int  col=2;
    int  row=1;
    assertEquals(true,OXProgram.checkWin(table,currentPlayer,row,col));
    }
    
    
    @Test
    public void testCheckX1PlayerOWin() {
    char table[][] = {{'O', '-', '-'},
                                {'-', 'O', '-'}, 
                                {'-', '-', 'O'}};
    char currentPlayer = 'O';
    
    assertEquals(true,OXProgram.checkX1(table,currentPlayer));
    }
    @Test
    public void testCheckX1PlayerXWin() {
    char table[][] = {{'X', '-', '-'},
                                {'-', 'X', '-'}, 
                                {'-', '-', 'X'}};
    char currentPlayer = 'X';
    
    assertEquals(true,OXProgram.checkX1(table,currentPlayer));
    }
    
    
    @Test
    public void testCheckX2PlayerOWin() {
    char table[][] = {{'-', '-', 'O'},
                                {'-', 'O', '-'}, 
                                {'O', '-', '-'}};
    char currentPlayer = 'O';
    
    assertEquals(true,OXProgram.checkX2(table,currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXWin() {
    char table[][] = {{'-', '-', 'X'},
                                {'-', 'X', '-'}, 
                                {'X', '-', '-'}};
    char currentPlayer = 'X';
    
    assertEquals(true,OXProgram.checkX2(table,currentPlayer));
    }
    
    
    @Test
    public void testCheckDrawPlayer() {
        int count=8;
    
    assertEquals(true,OXProgram.checkDraw(count));
    }
}
